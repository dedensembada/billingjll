<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
    {
        parent::__construct();
		$this->load->model("mdata");
		$this->load->helper(array('form', 'url','file'));
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory')); 
    }

	public function index()
	{
		$data['title'] = 'Anandamaya Residences';
		$this->load->view('vindex',$data);
	}
	
	function cekform()
	{
		$data['qcekform'] = $this->mdata->cekform();
		$this->load->view('vcekform',$data);
	}
	function cekform2()
	{
		$data['qcekform2'] = $this->mdata->cekform2();
		$this->load->view('vcekform2',$data);
	}

	function coba()
	{
		//$data['qinvoice'] = $this->mdata->qinvoice();
		$data['title'] = "Anandamaya Residences";
		$this->load->view('vcoba',$data);
	}

	function login()
	{
		$txtusername = $this->input->post("txtusername");
		$txtpassword = md5($this->input->post("txtpassword"));
		$qcekusername = $this->db->query("SELECT * FROM TUSER WHERE USERTOWER='$txtusername' AND USERPASSWORD='$txtpassword'");
		$valcekusername = $qcekusername->num_rows();
		if($valcekusername == 1){
			$row = $qcekusername->row();
			$userid = $row->USERID;
			$userlevel = $row->USERLEVEL;
			$this->load->library('session');
			$this->session->set_userdata('userid', $userid);
			$this->session->set_userdata('username', $txtusername);
			$this->session->set_userdata('userlevel', $userlevel);
			if($userlevel == 1){
				redirect('dashboard','refresh');
			}elseif($userlevel ==2){
				redirect('home','refresh');
			}
		}else{
			$data['title'] = "Anandamaya Residences";
			$data['error'] = "<h5 style='color:#ff0000;'>Invalid Username or Password!<br>Please Try Again!</h5>";
			$this->load->view('vindex',$data);
		}
	}

	function changepassword()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL)
		{
			$data['title'] = 'Anandamaya Residences';
			$userdata = $this->session->userdata('userlevel');
			if($userdata==1){
				$data['userid'] = $this->input->post('txtuserid');
			}elseif($userdata==2){
				$data['userid'] = $this->session->userdata('userid');
			}
			$this->load->view("vchangepassword",$data);
		}else{
			redirect('','refresh');
		}
	}

	function savepassword()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL){
			$data['title'] = 'Anandamaya Residences';
			$txtuserid = $this->input->post('txtuserid');
			$txtoldpassword = md5($this->input->post('txtoldpassword'));
			$txtnewpassword = md5($this->input->post('txtnewpassword'));
			$txtconfirmpassword = md5($this->input->post('txtconfirmpassword'));
			
			$qcekoldpassword = $this->db->query("SELECT * FROM TUSER WHERE USERID='$txtuserid'")->row();
			$cekoldpassword = $qcekoldpassword->USERPASSWORD;
			if($txtoldpassword != $cekoldpassword){
				$data['title'] = "Anandamaya Residences";
				$data['error'] = "<h5 style='color:#ff0000;'>Passwrod Lama Anda Salah!</h5>";
				$data['userid'] = $this->session->userdata('userid');
				$this->load->view('vchangepassword',$data);
			}elseif($txtnewpassword != $txtconfirmpassword){
				$data['title'] = "Anandamaya Residences";
				$data['error'] = "<h5 style='color:#ff0000;'>Konfirmasi Password Baru Anda Salah!</h5>";
				$data['userid'] = $this->session->userdata('userid');
				echo '<br>txtnewpassword'.$txtnewpassword;
				echo '<br>txtconfirmpassword'.$txtconfirmpassword;
				//$this->load->view('vchangepassword',$data);
			}else{
				$datapassword = array(
					'USERPASSWORD' => $txtconfirmpassword
				);
				$this->mdata->savePassword($txtuserid, $datapassword);
				$userdata = $this->session->userdata('userlevel');
				if($userdata==1){
					redirect('dashboard','refresh');
				}elseif($userdata==2){
					redirect('home','refresh');
				}
			}
		}else{
			redirect('','refresh');
		}
	}

	function home()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 2){
			$data['title'] = 'Anandamaya Residences';
			$data['userid'] = $this->session->userdata('userid');
			$id = $this->session->userdata('userid');
			$data['quser'] = $this->mdata->getUser($id);
			$data['qusers'] = $this->mdata->getUser($id);
			$data['qinvoice'] = $this->mdata->getInvoice($id);
			$data['qvirtual'] = $this->mdata->getVirtual($id);
			$this->load->view('vhome',$data);
		}else{
			redirect('','refresh');
		}
	}

	function dashboard()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 1){
			$data['title'] = 'Anandamaya Residences';
			$data['userid'] = $this->session->userdata('userid');
			$id = $this->session->userdata('userid');
			$data['username'] = $this->session->userdata('username');
			$data['qalluser'] = $this->mdata->getAllUser();

			$this->load->view('vdashboard',$data);
		}else{
			redirect('','refresh');
		}
	}

	function uploadbf()
	{
		$images = $this->input->post('userfile');
				
		$config['upload_path'] = './assets/images/bookingform/';
		$config['allowed_types'] = 'jpg|png|pdf';
		$config['max_size']	= '3000';
		$config['max_width']  = '2024';
		$config['max_height']  = '2068';
	
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload("userfile"))
		{
			//$error = array('error' => $this->upload->display_errors());
			$data['error'] = $this->upload->display_errors();
			$id = $this->input->post('txtuserid');
			$this->session->set_userdata('txtuserid', $id);
			echo $images."<br>";
			echo $this->upload->display_errors();
			//redirect('viewinvoice','refresh');
		}else{
			$upload_data = $this->upload->data(); 
			$file_name =   $upload_data['file_name'];
			$data = array(
				'USERBOOKINGFORM' => $file_name
			);
			$id = $this->input->post('txtuserid');
			$this->mdata->updateBookingForm($id, $data);
			
			$this->session->set_userdata('txtuserid', $id);
			echo "<script type='text/javascript'>alert('Upload Booking Form Berhasil!');</script>";
			redirect('viewinvoice','refresh');
		}
	}


	function uploaduof()
	{
		$images = $this->input->post('userfile');
				
		$config['upload_path'] = './assets/images/unitorderform/';
		$config['allowed_types'] = 'jpg|png|pdf';
		$config['max_size']	= '3000';
		$config['max_width']  = '2024';
		$config['max_height']  = '2068';
	
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload("userfile"))
		{
			//$error = array('error' => $this->upload->display_errors());
			$data['error'] = $this->upload->display_errors();
			$id = $this->input->post('txtuserid');
			$this->session->set_userdata('txtuserid', $id);
			echo $images."<br>";
			echo $this->upload->display_errors();
			//redirect('viewinvoice','refresh');
		}else{
			$upload_data = $this->upload->data(); 
			$file_name =   $upload_data['file_name'];
			$data = array(
				'USERUNITORDERFORM' => $file_name
			);
			$id = $this->input->post('txtuserid');
			$this->mdata->updateUnitOrderForm($id, $data);
			
			$this->session->set_userdata('txtuserid', $id);
			echo "<script type='text/javascript'>alert('Upload Unit Order Form Berhasil!');</script>";
			redirect('viewinvoice','refresh');
		}
	}

	function viewinvoice()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 1){
			$data['title'] = 'Anandamaya Residences';
			$txtuserid = $this->session->userdata('txtuserid');
			if(strlen($txtuserid)>0){
				$id = $txtuserid;
			}else{
				$id = $this->input->post('txtuserid');
			}
			$data['quser'] = $this->mdata->getUser($id);
			$qinvoice = $this->mdata->getInvoice($id);
			$data['countqinvoice'] = count($qinvoice);
			$data['qinvoice'] = $this->mdata->getInvoice($id);
			$data['qvirtual'] = $this->mdata->getVirtual($id);
			$data['txtuserid'] = $id;
			$data['username'] = $this->session->userdata('username');
			$this->load->view('vdetail',$data);
		}else{
			redirect('','refresh');
		}
	}

	function editkwitansi()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 1){
			$data['title'] = 'Anandamaya Residences';
			$id = $this->input->post('txtuserid');
			$invoiceid = $this->input->post('txtinvoiceid');
			$data['quser'] = $this->mdata->getUser($id);
			$data['qinvoicedetail'] = $this->mdata->getInvoiceDetail($invoiceid);
			$this->load->view('veditkwitansi',$data);
		}else{
			redirect('','refresh');
		}
	}

	function updatekwitansi()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 1){
			$id = $this->input->post('txtuserid');
			$txtinvoiceid = $this->input->post('txtinvoiceid');
			$txtinvoiceinfo = $this->input->post('txtinvoiceinfo');
			$txtinvoiceduedate = date("Y-m-d",strtotime($this->input->post('txtinvoiceduedate')));
			$txtinvoicetotal = $this->input->post('txtinvoicetotal');
			$txtinvoicefine = $this->input->post('txtinvoicefine');
			

			$images = $this->input->post('userfile');
				
			$config['upload_path'] = './assets/images/content/';
			$config['allowed_types'] = 'jpg|png|pdf';
			$config['max_size']	= '3000';
			$config['max_width']  = '2024';
			$config['max_height']  = '2068';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload("userfile"))
			{
				//$error = array('error' => $this->upload->display_errors());
				$data['error'] = $this->upload->display_errors();
				$id = $this->input->post('txtuserid');
				$invoiceid = $this->input->post('txtinvoiceid');
				$data['title'] = 'Anandamaya Residences';
				$data['quser'] = $this->mdata->getUser($id);
				$data['qinvoicedetail'] = $this->mdata->getInvoiceDetail($invoiceid);
				$this->load->view('veditinvoice', $data);
			}else{
				$upload_data = $this->upload->data(); 
				$file_name =   $upload_data['file_name'];
				$datainvoice = array(
					'INVOICEINFO' => $txtinvoiceinfo,
					'INVOICETOTAL' => $txtinvoicetotal,
					'INVOICEFINE' => $txtinvoicefine,
					'INVOICERECEIPT' => $file_name
				);
				$this->mdata->updateInvoice($txtinvoiceid, $datainvoice);
				

				$invoiceid = $txtinvoiceid;
				$qcheck = $this->db->query("SELECT * FROM TINVOICE WHERE INVOICEID='$invoiceid'")->row();
				$fine = $qcheck->INVOICEFINE;
				$receipt = $qcheck->INVOICERECEIPT;
				$lenreceipt = strlen($receipt);
				
				/*
				KONDISI 1 => DENDA ADA, KWITANSI ADA => BELUM LUNAS
				KONDISI 2 => DENDA ADA, KWITANSI TIDAK ADA => BELUM LUNAS
				KONDISI 3 => DENDA TIDAK ADA, KWITANSI ADA => LUNAS
				KONDISI 4 => DENDA TIDAK ADA, KWITANSI TIDAK ADA => BELUM LUNAS
				*/

				if($fine=='Ada' && $lenreceipt>0){
					$updatestatus = $this->db->query("UPDATE TINVOICE SET INVOICESTATUS='0' WHERE INVOICEID = $invoiceid");
				}elseif($fine=='Ada' && $lenreceipt<1){
					$updatestatus = $this->db->query("UPDATE TINVOICE SET INVOICESTATUS='0' WHERE INVOICEID = $invoiceid");
				}elseif($fine=='Tidak Ada' && $lenreceipt>0){
					$updatestatus = $this->db->query("UPDATE TINVOICE SET INVOICESTATUS='1' WHERE INVOICEID = $invoiceid");
				}elseif($fine=='Tidak Ada' && $lenreceipt<1){
					$updatestatus = $this->db->query("UPDATE TINVOICE SET INVOICESTATUS='0' WHERE INVOICEID = $invoiceid");
				}

				$txtuserid = $this->input->post('txtuserid');
				$this->session->set_userdata('txtuserid', $txtuserid);
				
				//redirect('viewinvoice',$data);
				echo "<script type='text/javascript'>alert('Upload Success!');</script>";
				//echo "<script type='text/javascript'>window.location.reload();</script>";
				

				redirect('viewinvoice','refresh');
			}
		}else{
			redirect('','refresh');
		}
	}

	function editinvoice()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 1){
			$data['title'] = 'Anandamaya Residences';
			$id = $this->input->post('txtuserid');
			$invoiceid = $this->input->post('txtinvoiceid');
			$data['quser'] = $this->mdata->getUser($id);
			$data['qinvoicedetail'] = $this->mdata->getInvoiceDetail($invoiceid);
			$this->load->view('veditinvoice',$data);
		}else{
			redirect('','refresh');
		}
	}

	function updateinvoice()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 1){
			$id = $this->input->post('txtuserid');
			$txtinvoiceid = $this->input->post('txtinvoiceid');
			$txtinvoiceinfo = $this->input->post('txtinvoiceinfo');
			$txtinvoiceduedate = date("Y-m-d",strtotime($this->input->post('txtinvoiceduedate')));
			$txtinvoicetotal = $this->input->post('txtinvoicetotal');
			$txtinvoicefine = $this->input->post('txtinvoicefine');
			
			$datainvoice = array(
					'INVOICEFINE' => $txtinvoicefine
			);
			$this->mdata->updateInvoice($txtinvoiceid, $datainvoice);
				 
			$invoiceid = $txtinvoiceid;
			$qcheck = $this->db->query("SELECT * FROM TINVOICE WHERE INVOICEID='$invoiceid'")->row();
			$fine = $qcheck->INVOICEFINE;
			$receipt = $qcheck->INVOICERECEIPT;
			$lenreceipt = strlen($receipt);

			/*
			KONDISI 1 => DENDA ADA, KWITANSI ADA => BELUM LUNAS
			KONDISI 2 => DENDA ADA, KWITANSI TIDAK ADA => BELUM LUNAS
			KONDISI 3 => DENDA TIDAK ADA, KWITANSI ADA => LUNAS
			KONDISI 4 => DENDA TIDAK ADA, KWITANSI TIDAK ADA => BELUM LUNAS
			*/

			if($fine=='Ada' && $lenreceipt>0){
				$updatestatus = $this->db->query("UPDATE TINVOICE SET INVOICESTATUS='0' WHERE INVOICEID = $invoiceid");
			}elseif($fine=='Ada' && $lenreceipt<1){
				$updatestatus = $this->db->query("UPDATE TINVOICE SET INVOICESTATUS='0' WHERE INVOICEID = $invoiceid");
			}elseif($fine=='Tidak Ada' && $lenreceipt>0){
				$updatestatus = $this->db->query("UPDATE TINVOICE SET INVOICESTATUS='1' WHERE INVOICEID = $invoiceid");
			}elseif($fine=='Tidak Ada' && $lenreceipt<1){
				$updatestatus = $this->db->query("UPDATE TINVOICE SET INVOICESTATUS='0' WHERE INVOICEID = $invoiceid");
			}
			$txtuserid = $this->input->post('txtuserid');
			$this->session->set_userdata('txtuserid', $txtuserid);
			
			//redirect('viewinvoice',$data);
			//echo "<script type='text/javascript'>alert('Status Pembayaran menjadi DONE!');</script>";
			//echo "<script type='text/javascript'>window.location.reload();</script>";
			

			redirect('viewinvoice','refresh');
			
		}else{
			redirect('','refresh');
		}
	}

	function download(){
		$this->load->helper('download');
		
		$txttype = $this->input->post('txttype');
		
		if($txttype=='invoice'){
			$txtinvoiceid = $this->input->post('txtinvoiceid');
			$qfileinvoice = $this->db->query("SELECT * FROM TINVOICE WHERE INVOICEID='$txtinvoiceid'")->row();
			$fileinvoice = $qfileinvoice->INVOICERECEIPT;
			$file_url = 'assets/images/content/'.$fileinvoice;
		}elseif($txttype=='booking'){
			$txtinvoiceid = $this->input->post('txtuserid');
			$qfileinvoice = $this->db->query("SELECT * FROM TUSER WHERE USERID='$txtinvoiceid'")->row();
			$fileinvoice = $qfileinvoice->USERBOOKINGFORM;
			$file_url = 'assets/images/bookingform/'.$fileinvoice;
		}elseif($txttype=='unitorder'){
			$txtinvoiceid = $this->input->post('txtuserid');
			$qfileinvoice = $this->db->query("SELECT * FROM TUSER WHERE USERID='$txtinvoiceid'")->row();
			$fileinvoice = $qfileinvoice->USERUNITORDERFORM;
			$file_url = 'assets/images/unitorderform/'.$fileinvoice;
		}
		
		if (file_exists($file_url)) { 
			$data = file_get_contents($file_url); // Read the file's contents
			$name = $fileinvoice;

			force_download($name, $data);
		}else{
			echo '<script type="text/javascript">alert("File not exists!");</script>';
			echo '<script type="text/javascript">window.history.back();</script>';
		}
		/*$txttype = $this->input->post('txttype');
		$txtinvoiceid = $this->input->post('txtinvoiceid');
			$qfileinvoice = $this->db->query("SELECT * FROM TINVOICE WHERE INVOICEID='$txtinvoiceid'")->row();
			$fileinvoice = $qfileinvoice->INVOICERECEIPT;
			$file_url = base_url().'assets/images/content/'.$fileinvoice;
		/*if($txttype=='invoice'){
			$txtinvoiceid = $this->input->post('txtinvoiceid');
			$qfileinvoice = $this->db->query("SELECT * FROM TINVOICE WHERE INVOICEID='$txtinvoiceid'")->row();
			$fileinvoice = $qfileinvoice->INVOICERECEIPT;
			$file_url = base_url().'assets/images/content/'.$fileinvoice;
		}elseif($txttype=='booking'){
			$txtinvoiceid = $this->input->post('txtuserid');
			$qfileinvoice = $this->db->query("SELECT * FROM TUSER WHERE USERID='$txtinvoiceid'")->row();
			$fileinvoice = $qfileinvoice->USERBOOKINGFORM;
			$file_url = base_url().'assets/images/bookingform/'.$fileinvoice;
		}elseif($txttype=='unitorder'){
			$txtinvoiceid = $this->input->post('txtuserid');
			$qfileinvoice = $this->db->query("SELECT * FROM TUSER WHERE USERID='$txtinvoiceid'")->row();
			$fileinvoice = $qfileinvoice->USERUNITORDERFORM;
			$file_url = base_url().'assets/images/unitorderform/'.$fileinvoice;
		}
		
		if(strlen($fileinvoice)>1){
			ignore_user_abort(true);
			set_time_limit(0); // disable the time limit for this script

			$path = $file_url; // change the path to fit your websites document structure
			$dl_file = preg_replace("([^\w\s\d\-_~,;:\[\]\(\].]|[\.]{2,})", '', $_GET['download_file']); // simple file name validation
			$dl_file = filter_var($dl_file, FILTER_SANITIZE_URL); // Remove (more) invalid characters
			$fullPath = $path.$dl_file;

			if ($fd = fopen ($fullPath, "r")) {
			    $fsize = filesize($fullPath);
			    $path_parts = pathinfo($fullPath);
			    $ext = strtolower($path_parts["extension"]);
			    switch ($ext) {
			        case "pdf":
			        header("Content-type: application/pdf");
			        header("Content-Disposition: attachment; filename=\"".$path_parts["basename"]."\""); // use 'attachment' to force a file download
			        break;
			        // add more headers for other content types here
			        default;
			        header("Content-type: application/octet-stream");
			        header("Content-Disposition: filename=\"".$path_parts["basename"]."\"");
			        break;
			    }
			    header("Content-length: $fsize");
			    header("Cache-control: private"); //use this to open files directly
			    while(!feof($fd)) {
			        $buffer = fread($fd, 2048);
			        echo $buffer;
			    }
			}
			fclose ($fd);
			exit;
		}else{
			
			echo '<script type="text/javascript">alert("File not exists!");</script>';
			echo '<script type="text/javascript">window.history.back();</script>';

		}*/
	}


	function downloadinvoice(){
		
		
		$txtinvoiceid = $this->input->post('txtinvoiceid');
		$qfileinvoice = $this->db->query("SELECT * FROM TINVOICE WHERE INVOICEID='$txtinvoiceid'")->row();
		$fileinvoice = $qfileinvoice->INVOICERECEIPT;
		$file_url = base_url().'assets/images/content/'.$fileinvoice;
		
		
		
		if(strlen($fileinvoice)>1){
			ignore_user_abort(true);
			set_time_limit(0); // disable the time limit for this script

			$path = $file_url; // change the path to fit your websites document structure
			$dl_file = preg_replace("([^\w\s\d\-_~,;:\[\]\(\].]|[\.]{2,})", '', $_GET['download_file']); // simple file name validation
			$dl_file = filter_var($dl_file, FILTER_SANITIZE_URL); // Remove (more) invalid characters
			$fullPath = $path.$dl_file;

			if ($fd = fopen ($fullPath, "r")) {
			    $fsize = filesize($fullPath);
			    $path_parts = pathinfo($fullPath);
			    $ext = strtolower($path_parts["extension"]);
			    switch ($ext) {
			        case "pdf":
			        header("Content-type: application/pdf");
			        header("Content-Disposition: attachment; filename=\"".$path_parts["basename"]."\""); // use 'attachment' to force a file download
			        break;
			        // add more headers for other content types here
			        default;
			        header("Content-type: application/octet-stream");
			        header("Content-Disposition: filename=\"".$path_parts["basename"]."\"");
			        break;
			    }
			    header("Content-length: $fsize");
			    header("Cache-control: private"); //use this to open files directly
			    while(!feof($fd)) {
			        $buffer = fread($fd, 2048);
			        echo $buffer;
			    }
			}
			fclose ($fd);
			exit;
		}else{
			
			echo '<script type="text/javascript">alert("File not exists!");</script>';
			echo '<script type="text/javascript">window.history.back();</script>';

		}
	}

	function deletekwitansi()
	{
		$txtinvoicereceipt = $this->input->post('txtinvoicereceipt');
		$qcheck = $this->db->query("SELECT * FROM TINVOICE WHERE INVOICERECEIPT='$txtinvoicereceipt'")->row();
		$invoiceid = $qcheck->INVOICEID;

		$datainvoice = array(
			'INVOICERECEIPT' => ''
		);
		$this->mdata->deleteKwitansi($txtinvoicereceipt, $datainvoice);
		$path = 'assets/images/content/'.$txtinvoicereceipt;
		/*DIREMARK TANPA MENGGUNAKAN UNLINK */
		//@chmod(base_url().'assets/images/content/'.$txtinvoicereceipt, 0777);
		//@unlink(base_url().'assets/images/content/'.$txtinvoicereceipt);
		//delete_files($path);
		//unlink($path);
		$data['title'] = 'Anandamaya Residences';
		$id = $this->input->post('txtuserid');
		$invoiceid = $this->input->post('txtinvoiceid');
		$data['quser'] = $this->mdata->getUser($id);
		$data['qinvoicedetail'] = $this->mdata->getInvoiceDetail($invoiceid);
		$data['qinvoice'] = $this->mdata->getInvoice($id);

		
		$updatestatus = $this->db->query("UPDATE TINVOICE SET INVOICESTATUS='0' WHERE INVOICEID = $invoiceid");
		$txtuserid = $this->input->post('txtuserid');
		$this->session->set_userdata('txtuserid', $txtuserid);
		redirect('viewinvoice','refresh');
		//$this->load->view('vdetail',$data);		
	}


	function deletebookingform()
	{
		$txtuserid = $this->input->post('txtuserid');
		
		$data = array(
			'USERBOOKINGFORM' => ''
		);
		$this->mdata->deleteBookingForm($txtuserid, $data);
		redirect('viewinvoice','refresh');		
	}

	function deleteunitorderform()
	{
		$txtuserid = $this->input->post('txtuserid');
		
		$data = array(
			'USERUNITORDERFORM' => ''
		);
		$this->mdata->deleteUnitOrderForm($txtuserid, $data);
		redirect('viewinvoice','refresh');		
	}

	function adduser()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 1){
			$data['title'] = 'Anandamaya Residences';
			$data['getTowerAvailable'] = $this->mdata->getTowerAvailable();
			$this->load->view('vadduser',$data);
		}else{
			redirect('','refresh');
		}
	}

	function saveuser()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 1){
			$data['title'] = 'Anandamaya Residences';
			$txttower = $this->input->post('txttower');
			$txtnama = $this->input->post('txtnama');
			$txttanggaltransaksi = date("Y-m-d",strtotime($this->input->post('txttanggaltransaksi')));
			$txtharga = $this->input->post('txtharga');
			$txtpassword = md5($this->input->post('txtpassword'));
			$datauser = array(
				'USERTOWER' => $txttower,
				'USERNAME' => $txtnama,
				'USERPASSWORD' => $txtpassword,
				'USERTRANSACTIONDATE' => $txttanggaltransaksi,
				'USERBINDING' => $txtharga,
				'USERLEVEL' => 2
			);
			$this->mdata->saveUser($datauser);
			redirect('dashboard','refresh');
		}else{
			redirect('','refresh');
		}
	}

	function addinvoice()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 1){
			$data['title'] = 'Anandamaya Residences';
			$userid = $this->input->post('txtuserid');
			$data['userid'] = $userid;
			$qusername = $this->db->query("SELECT * FROM TUSER WHERE USERID='$userid'")->row();
			$username = $qusername->USERNAME;
			$tower = $qusername->USERTOWER;
			$data['username'] = $username;
			$data['tower'] = $tower;
			$this->load->view('vaddinvoice',$data);
		}else{
			redirect('','refresh');
		}
	}

	function saveinvoice()
	{
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 1){
			$data['title'] = 'Anandamaya Residences';
			$txtuserid = $this->input->post('txtuserid');
			$txtinfo = $this->input->post('txtinfo');
			$txtduedate = date("Y-m-d",strtotime($this->input->post('txtduedate')));
			$txtfine = $this->input->post('txtfine');
			$txttotal = $this->input->post('txttotal');
			$datainvoice = array(
				'USERID' => $txtuserid,
				'INVOICEINFO' => $txtinfo,
				'INVOICEDUEDATE' => $txtduedate,
				'INVOICEFINE' => $txtfine,
				'INVOICETOTAL' => $txttotal,
				'INVOICESTATUS' => 0,
				'INVOICEPUBLISH' => 1
			);
			$this->mdata->saveInvoice($datainvoice);
			redirect('dashboard','refresh');
		}else{
			redirect('','refresh');
		}
	}

	function deleteinvoice()
	{
		$invoiceid = $this->input->post('txtinvoiceid');
		$datainvoice = array(
				'INVOICEPUBLISH' => 0
			);
		$this->mdata->deleteInvoice($invoiceid, $datainvoice);
		redirect('dashboard','refresh');
	}

	function deleteuser()
	{
		$userid = $this->input->post('txtuserid');
		$datauser = array(
				'USERACTIVE' => 0
			);
		$this->mdata->deleteUser($userid, $datauser);
		redirect('dashboard','refresh');
	}

	function importinvoice()
	{
		$data['title'] = 'Anandamaya Residences';
		$this->load->view('vimportinvoice',$data);
	}

	function importexcel() 
	{ 
		if($this->session->userdata('userlevel')!='' || $this->session->userdata('userlevel')!=NULL && $this->session->userdata('userlevel') == 1)
		{ 
			$fileexcel = $this->input->post('userfile');

			$config['upload_path'] = './assets/excel/';
			$config['allowed_types'] = 'xls';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('userfile')) 
			{
				$data = array('error' => $this->upload->display_errors()); 
			}else{
				$data = array('error' => false); 
				$upload_data = $this->upload->data();            
				$this->load->library('excel_reader'); 
				$this->excel_reader->setOutputEncoding('230787'); 
				$file =  $upload_data['full_path']; 
				$this->excel_reader->read($file); 
				error_reporting(E_ALL ^ E_NOTICE); 
				$data = $this->excel_reader->sheets[0] ;                        
				$dataexcel = Array(); 
				for ($i = 2; $i <= $data['numRows']; $i++) {                            
					if($data['cells'][$i][1] == '') break;                             
					
					$dataexcel[$i-1]['INVOICETOWER'] = $data['cells'][$i][1];                            
					$dataexcel[$i-1]['INVOICEINFO'] = $data['cells'][$i][2];                             
					$dataexcel[$i-1]['INVOICEDUEDATE'] = $data['cells'][$i][3];                             
					$dataexcel[$i-1]['INVOICETOTAL'] = $data['cells'][$i][4];                             
					//$dataexcel[$i-1]['INVOICEPUBLISH'] = $data['cells'][$i][5];                             
					delete_files($upload_data['file_path']);             
					//$this->mdata->importexcel($dataexcel); 
					$invoicetower = $dataexcel[$i-1]['INVOICETOWER'];
		            $quserid = $this->db->query("SELECT * FROM TUSER WHERE USERTOWER='$invoicetower'")->row();
		            $userid = $quserid->USERID;
		            /*$data = array(                 
		                'USERID'=> $userid,                 
		                'INVOICEINFO'=>$dataexcel[$i-1]['INVOICEINFO'],                 
		                'INVOICEDUEDATE'=>$dataexcel[$i-1]['INVOICEDUEDATE'],                
		                'INVOICETOTAL'=>$dataexcel[$i-1]['INVOICETOTAL'],              
		                'INVOICEPUBLISH'=>$dataexcel[$i-1]['INVOICEPUBLISH']              
		             );            
	             	//$this->db->insert('TINVOICE', $data);  */ 
					$invoiceinfo = $dataexcel[$i-1]['INVOICEINFO'];
					$duedate = $dataexcel[$i-1]['INVOICEDUEDATE'];
					$y = substr($duedate,-4);
					$m = substr($duedate,3,2);
					$d = substr($duedate,0,2);
					$invoiceduedate = $y.'-'.$m.'-'.$d;
					$invoicetotal = $dataexcel[$i-1]['INVOICETOTAL'];
					//$invoicepublish = $dataexcel[$i-1]['INVOICEPUBLISH'];
	             	$this->db->query("INSERT INTO TINVOICE (USERID, INVOICEINFO, INVOICEDUEDATE, INVOICETOTAL, INVOICEPUBLISH) VALUES ('$userid','$invoiceinfo','$invoiceduedate','$invoicetotal','1')");      
		           	echo $userid.'+';
		           	echo $invoiceinfo.'+';
		           	echo $duedate.'+';
		           	echo $invoiceduedate.'+';
		           	echo $invoicetotal.'+';
		           	//echo $invoicepublish.'+';
		           	echo "<br>";
					
				}
				echo "UPLOAD SUCCESS";
				//break;
				//header('location:'.base_url().'dashboard'); 
			
			}
			echo $data = $this->upload->display_errors(); 
			//break;
			header('location:'.base_url().'dashboard'); 
		}else{ 
			header('location:'.base_url().'dashboard'); 
		} 
	}

	function exportinvoice() {
		$this->load->library('excel'); 
        $this->excel->setActiveSheetIndex(0);
        // Gets all the data using MY_Model.php
        $id = $this->input->post('txtuserid');
        $data = $this->mdata->export($id);

        $this->excel->stream('export_invoice_webar.xls', $data);
    }

    function exportallinvoice() {
		$this->load->library('excel'); 
        $this->excel->setActiveSheetIndex(0);
        // Gets all the data using MY_Model.php
        $data = $this->mdata->exportall();

        $this->excel->stream('export_invoice_webar.xls', $data);
    }

    function exportallinvoices() {
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
    
		 $objPHPExcel = new PHPExcel();
 
                // Set properties
            $objPHPExcel->getProperties()
                  ->setCreator("SMA Insan Cendekia Alkautsar") //creator
                    ->setTitle("Jadwal pelajaran");  //file title
 
        $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
        $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
        $objget->setTitle('Sample Sheet'); //sheet title
        $objset->setCellValue('A1',"This is Sample Excel File"); //insert cell value
        $objget->getStyle('A1')->getFont()->setBold(true)  // set font weight
                ->setSize(15);    //set font size
 
        //table header
        $cols = array("A","B","C","D","E","F");
                $val = array("No","Member ID","Member Username","Member Address","Member Phone","Member Status");
        for ($a=0;$a<6;$a++) 
                {
                    $objset->setCellValue($cols[$a].'3', $val[$a]);
                        //set borders
            $objget->getStyle($cols[$a].'3')->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objget->getStyle($cols[$a].'3')->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objget->getStyle($cols[$a].'3')->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objget->getStyle($cols[$a].'3')->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
 
            //set alignment
            $objget->getStyle($cols[$a].'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            //set font weight
            $objget->getStyle($cols[$a].'3')->getFont()->setBold(true) ;
        }
                
               //taruh baris data disini
 				//taruh baris data disini
                //simpan dalam file sample.xls
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');                
                $objWriter->save('sample.xls');
    }

	function logout(){
		$this->session->sess_destroy();
		redirect('','refresh');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */