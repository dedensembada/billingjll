<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "front";
$route['login'] = "front/login";
$route['home'] = "front/home";
$route['dashboard'] = "front/dashboard";
$route['viewinvoice'] = "front/viewinvoice";
$route['editinvoice'] = "front/editinvoice";
$route['editkwitansi'] = "front/editkwitansi";
$route['updateinvoice'] = "front/updateinvoice";
$route['updatekwitansi'] = "front/updatekwitansi";
$route['addinvoice'] = "front/addinvoice";
$route['saveinvoice'] = "front/saveinvoice";
$route['deleteinvoice'] = "front/deleteinvoice";
$route['deleteuser'] = "front/deleteuser";
$route['deletekwitansi'] = "front/deletekwitansi";
$route['adduser'] = "front/adduser";
$route['saveuser'] = "front/saveuser";
$route['download'] = "front/download";
$route['changepassword'] = "front/changepassword";
$route['savepassword'] = "front/savepassword";
$route['logout'] = "front/logout";
$route['uploadbf'] = "front/uploadbf";
$route['uploaduof'] = "front/uploaduof";
$route['downloadbookingform'] = "front/downloadbookingform";
$route['downloadunitorderform'] = "front/downloadunitorderform";
$route['deletebookingform'] = "front/deletebookingform";
$route['deleteunitorderform'] = "front/deleteunitorderform";
$route['importinvoice'] = "front/importinvoice";
$route['importexcel'] = "front/importexcel";
$route['downloadinvoice'] = "front/downloadinvoice";
$route['exportinvoice'] = "front/exportinvoice";
$route['exportallinvoice'] = "front/exportallinvoice";
$route['cekform'] = "front/cekform";
$route['cekform2'] = "front/cekform2";
$route['coba'] = "front/coba";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */