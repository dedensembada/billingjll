<?php
class Mdata extends CI_Model{
	
	function getInvoice($id) {
        $this->db->from('TINVOICE');
        $this->db->where('USERID',$id);
        $this->db->where('INVOICEPUBLISH','1');
		$this->db->order_by('INVOICEDUEDATE','ASC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function getVirtual($id) {
        $this->db->from('TVIRTUALACCOUNT');
        $this->db->join('TUSER','TVIRTUALACCOUNT.VIRTUALACCOUNTTOWER=TUSER.USERTOWER');
        $this->db->where('USERID',$id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function getInvoiceDetail($invoiceid) {
        $this->db->from('TINVOICE');
        $this->db->where('INVOICEID',$invoiceid);
		$this->db->where('INVOICEPUBLISH','1');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function getUser($id) {
        $this->db->from('TUSER');
        $this->db->where('USERID',$id);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	
    function qinvoice() {
        $query = $this->db->query("SELECT * FROM TINVOICE,TUSER WHERE TINVOICE.USERID = TUSER.USERID AND TINVOICE.INVOICEINFO LIKE 'Angsuran 2/%' ORDER BY TINVOICE.USERID ASC");
        //$query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function cekform() {
        $this->db->from('TUSER');
        $this->db->where('USERLEVEL','2');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    function cekform2() {
        $query = $this->db->query("SELECT * FROM TINVOICE,TUSER WHERE TINVOICE.USERID = TUSER.USERID AND TINVOICE.INVOICEINFO LIKE '%Angsuran%' ORDER BY TINVOICE.USERID");
        
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

	function getAllUser() {
        $this->db->from('TUSER');
        $this->db->where('USERLEVEL','2');
        $this->db->where('USERACTIVE','1');
        $this->db->order_by('USERTOWER','ASC');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function export($id) {
        $this->db->select(' TUSER.USERTOWER AS Tower,
                            TINVOICE.INVOICEINFO AS Keterangan,
                            TINVOICE.INVOICEDUEDATE AS TglJatuhTempo,
                            TINVOICE.INVOICEFINE AS Denda,
                            TINVOICE.INVOICETOTAL AS Jumlah, 
                            TINVOICE.INVOICERECEIPT AS Kwitansi, 
                            TUSER.USERBOOKINGFORM AS BookingForm, 
                            TUSER.USERUNITORDERFORM AS UnitOrderForm,
                            TINVOICE.INVOICESTATUS AS Status');
        $this->db->from('TINVOICE');
        $this->db->join('TUSER','TINVOICE.USERID=TUSER.USERID');
        $this->db->where('TINVOICE.USERID',$id);
        $this->db->where('TINVOICE.INVOICEPUBLISH','1');
        $this->db->order_by('INVOICEDUEDATE','ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function exportall() {
        $this->db->select(' TUSER.USERTOWER AS Tower,
                            TINVOICE.INVOICEINFO AS Keterangan,
                            TINVOICE.INVOICEDUEDATE AS TglJatuhTempo,
                            TINVOICE.INVOICETOTAL AS Jumlah, 
                            TINVOICE.INVOICERECEIPT AS Kwitansi, 
                            TUSER.USERBOOKINGFORM AS BookingForm, 
                            TUSER.USERUNITORDERFORM AS UnitOrderForm
                            ');
        $this->db->from('TINVOICE');
        $this->db->join('TUSER','TINVOICE.USERID=TUSER.USERID');
        $this->db->where('TINVOICE.INVOICEPUBLISH','1');
        $this->db->order_by('TUSER.USERTOWER','ASC');
        $this->db->order_by('TINVOICE.INVOICEDUEDATE','ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function getTowerAvailable() {
        $query = $this->db->query('SELECT * FROM TVIRTUALACCOUNT WHERE VIRTUALACCOUNTTOWER NOT IN (SELECT USERTOWER FROM TUSER) ORDER BY VIRTUALACCOUNTTOWER ASC');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function updateInvoice($txtinvoiceid, $datainvoice) {
		$this->db->where('INVOICEID', $txtinvoiceid);
		$result = $this->db->update('TINVOICE', $datainvoice); 
		return $result;
    }

    function savePassword($txtuserid, $datapassword) {
		$this->db->where('USERID', $txtuserid);
		$result = $this->db->update('TUSER', $datapassword); 
		return $result;
    }

	function deleteKwitansi($txtinvoicereceipt, $datainvoice) {
		$this->db->where('INVOICERECEIPT', $txtinvoicereceipt);
		$result = $this->db->update('TINVOICE', $datainvoice); 
		return $result;
    }

    function deleteInvoice($invoiceid, $datainvoice) {
		$this->db->where('INVOICEID', $invoiceid);
		$result = $this->db->update('TINVOICE', $datainvoice); 
		return $result;
    }

    function deleteUser($userid, $datauser) {
		$this->db->where('USERID', $userid);
		$result = $this->db->update('TUSER', $datauser); 
		return $result;
    }

    function saveUser($datauser) {
    	$this->db->insert('TUSER', $datauser);
    }

    function saveInvoice($datainvoice) {
    	$this->db->insert('TINVOICE', $datainvoice);
    }
	
    function updateBookingForm($id, $data) {
        $this->db->where('USERID', $id);
        $result = $this->db->update('TUSER', $data); 
        return $result;
    }

    function updateUnitOrderForm($id, $data) {
        $this->db->where('USERID', $id);
        $result = $this->db->update('TUSER', $data); 
        return $result;
    }

    function deleteBookingForm($id, $data) {
        $this->db->where('USERID', $id);
        $result = $this->db->update('TUSER', $data); 
        return $result;
    }

    function deleteUnitOrderForm($id, $data) {
        $this->db->where('USERID', $id);
        $result = $this->db->update('TUSER', $data); 
        return $result;
    }


    function importexcel($dataexcel)     
    {         
        for($i=1;$i<2;$i++){            
            $invoicetower = $dataexcel[$i]['INVOICETOWER'];
            $quserid = $this->db->query("SELECT * FROM TUSER WHERE USERTOWER='$invoicetower'")->row();
            $userid = $quserid->USERID;
            $data = array(                 
                'USERID'=> $userid,                 
                'INVOICEINFO'=>$dataexcel[$i]['INVOICEINFO'],                 
                'INVOICEDUEDATE'=>$dataexcel[$i]['INVOICEDUEDATE'],                
                'INVOICETOTAL'=>$dataexcel[$i]['INVOICETOTAL'],              
                'INVOICEPUBLISH'=>$dataexcel[$i]['INVOICEPUBLISH']              
                       
             );            
             $this->db->insert('TINVOICE', $data);         
        }

    }

    /*
	
	CREATE TABLE TVIRTUALACCOUNT (
		VIRTUALACCOUNTID INT NOT NULL AUTO_INCREMENT,
		VIRTUALACCOUNTTOWER VARCHAR(255),
		VIRTUALACCOUNTNAME VARCHAR(255),
		VIRTUALACCOUNTBANK VARCHAR(255),
		VIRTUALACCOUNTNO VARCHAR(255),
		PRIMARY KEY (VIRTUALACCOUNTID)
	)

	ALTER TABLE TUSER ADD COLUMN USERBOOKINGFORM VARCHAR(255);
	ALTER TABLE TUSER ADD COLUMN USERUNITORDERFORM VARCHAR(255);


	*/
}	
?>
