<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
   
    <title>ANANDAMAYA RESIDENCES</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="ANANDAMAYA RESIDENCES">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta property="og:site_name" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:description" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:image" content="<?php echo base_url();?>assets/images/facebook.jpg"/>
    <meta property="og:url" content="" />
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="<?php echo base_url();?>assets/ico/favicon.png">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
          <div class="panels">

            <div class="logo">
              <img src="<?php echo base_url();?>assets/images/anandamaya-logo.jpg">
            </div>
          </div>
        </div>
      </div>
      <br><br>
      <div class="panels">
        <div class="row">
          <div class="col-lg-12">
            <div class="content">
              <div class="logout text-right">
                <a href="<?php echo base_url();?>logout" class="btn btn-danger">Keluar</a>
                <hr>
              </div>
              <div class="title text-center">
                <h2>Jadwal Pembayaran</h2>
              </div> 

            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6">
            <div class="content">
              <?php foreach($quser as $quser): ?>
              <table class="table table-hover">
                <tr>
                  <td>Nama</td>
                  <td>:</td>
                  <td><?php echo $quser->USERNAME; ?></td>
                </tr>
                <tr>
                  <td>Tanggal Transaksi</td>
                  <td>:</td>
                  <td><?php echo date("d/m/Y",strtotime($quser->USERTRANSACTIONDATE)); ?></td>
                </tr>
                <tr>
                  <td>Unit No.</td>
                  <td>:</td>
                  <td><?php echo $quser->USERTOWER; ?></td>
                </tr>
                <tr>
                  <td>Harga Pengikatan</td>
                  <td>:</td>
                  <td><?php echo 'Rp. ' . number_format( $quser->USERBINDING, 0 , '' , '.' ) . ''; ?></td>
                </tr>
                <tr>
                  <td colspan="3">
                    <form action="<?php echo base_url();?>changepassword" method="POST">
                        <input type="submit" class="btn btn-default" value="Ubah Password">
                    </form>
                  </td>
                </tr>
              </table>
            </div>
          </div>
           <?php endforeach; ?>
          <?php foreach($qvirtual as $qvirtual):?>
          <div class="col-lg-6 text-right">
            <div class="content">
              <table class="table table-hover">
                <tr>
                  <td>Pembayaran ditujukan ke :</td>
                </tr>
                <tr>
                  <td><?php echo $qvirtual->VIRTUALACCOUNTNAME; ?></td>
                </tr>
                <tr>
                  <td>BANK <?php echo $qvirtual->VIRTUALACCOUNTBANK; ?></td>
                </tr>
                <tr>
                  <td>Virtual Account <?php echo $qvirtual->VIRTUALACCOUNTNO; ?></td>
                </tr>
              </table>
           
            </div>
          </div>
        <?php endforeach; ?>
        </div>
        <br>
        <div class="row">
          <div class="col-lg-12">
            <div class="content" style="margin-top:-50px;">
              Keterangan:<br>
              <ul>
                  <li>Jika tanggal jatuh tempo adalah hari Sabtu / Minggu / Libur, maka tanggal jatuh tempo akan jatuh pada hari kerja terakhir sebelum hari tersebut.</li>
                  <li>Jika terdapat perbedaan informasi, data yang benar mengacu kepada UOF asli.</li>
                  <li>Harga termasuk PPN, PPnBM & Pajak Barang Sangat Mewah (jika ada).</li>
                  <li>Jika terdapat perbedaan pembayaran dengan tampilan di Modul Jadwal Pembayaran, mohon agar dapat dikonfirmasi ke 021 - 571 1111.</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6">
            <div class="content">
              <?php foreach($qusers as $quser): ?>
              <table class="table table-bordered">
                <tr>
                  <td>Booking Form</td>
                  <td class="text-center">
                    <?php if(strlen($quser->USERBOOKINGFORM)>0){?>
                      <form action="<?php echo base_url();?>download" method="post">
                          <?php echo '<h6>'.$quser->USERBOOKINGFORM.'</h6>'; ?>
                          <input type="hidden" name="txtuserid" value="<?php echo $quser->USERID; ?>">
                          <input type="hidden" name="txttype" value="booking">
                          <input class="btn btn-default" type="submit" name="download" value="Download">
                        </form>
                    <?php }else{ ?>
                    &nbsp; - &nbsp;
                    <?php } ?>
                    
                  </td>
                </tr>
                <tr>
                    <td>Unit Order Form</td>
                    <td class="text-center">
                      <?php if(strlen($quser->USERUNITORDERFORM)>0){?>
                       <form action="<?php echo base_url();?>download" method="post">
                          <?php echo '<h6>'.$quser->USERUNITORDERFORM.'</h6>'; ?>
                          <input type="hidden" name="txtuserid" value="<?php echo $quser->USERID; ?>">
                          <input type="hidden" name="txttype" value="unitorder">
                          <input class="btn btn-default" type="submit" name="download" value="Download">
                        </form>
                     
                      <?php }else{ ?>
                       &nbsp; - &nbsp;
                      <?php } ?>
                    </td>
                    
                </tr>
              </table>
            <?php endforeach; ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
             <div class="content">
               <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Keterangan</th>
                        <th class="text-center">Tanggal Jatuh Tempo</th>
                        <th class="text-center">Jumlah</th>
                        <th class="text-center">Denda</th>
                        <th>Status</th>
                        <th>Kwitansi</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php 
                      $counter=1;
                      foreach($qinvoice as $qinvoice):
                    ?>
                    <tr>
                      <td><?php echo $counter; ?></td>
                      <td><?php echo $qinvoice->INVOICEINFO; ?></td>
                      <td class="text-center"><?php echo date("d/m/Y",strtotime($qinvoice->INVOICEDUEDATE)); ?></td>
                      <td class="text-left"><?php echo 'Rp. <span style="float:right;">' . number_format( $qinvoice->INVOICETOTAL, 0 , '' , '.' ) . '</span>'; ?></td>
                      <td><?php 
                            if($qinvoice->INVOICEFINE=='Ada'){
                              echo 'Ada';
                            }elseif($qinvoice->INVOICEFINE=='Tidak Ada'){
                              echo 'Tidak Ada';
                            }
                          ?>
                      </td> 
                      <td>
                        <?php 
                          if($qinvoice->INVOICESTATUS==1){
                            echo "Lunas";
                          }else{
                            echo 'Belum Lunas';
                          }
                        ?>
                      </td>
                      <td class="text-center">
                          <?php if(strpos($qinvoice->INVOICEINFO,'Booking')!==FALSE){ ?>
                          &nbsp - &nbsp;
                          <?php }elseif(strlen($qinvoice->INVOICERECEIPT)>1 && $qinvoice->INVOICESTATUS == 0){ ?>
                          <?php echo '<h6>'.$qinvoice->INVOICERECEIPT.'</h6>'; ?>
                          <?php }elseif(strlen($qinvoice->INVOICERECEIPT)>1 && $qinvoice->INVOICESTATUS == 1){ ?>
                          <form action="<?php echo base_url();?>download/" method="post">
                            <?php echo '<h6>'.$qinvoice->INVOICERECEIPT.'</h6>'; ?>
                            <input type="hidden" name="txtinvoiceid" value="<?php echo $qinvoice->INVOICEID; ?>">
                            <input type="hidden" name="txttype" value="invoice">
                            <input class="btn btn-default" type="submit" name="download" value="Download">
                          </form>
                          <?php }else{ echo '&nbsp'; } ?>
                      </td>
                    </tr>
                    <?php 
                      $counter++;
                      endforeach;
                    ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="content text-center">
              <div class="footer">
                <br>Copyright &copy; 2015 . Anandamaya Residences . <br><br>
              </div>
             
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/morris/morris.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/js/sb-admin.js"></script>
    <script>
      $(document).ready(function() {
         $('#dataTables-example').dataTable( {
           "paginate": false
         } );
      });
    </script>
  </body>
</html>