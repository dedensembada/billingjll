<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
   
   <title>ANANDAMAYA RESIDENCES</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="ANANDAMAYA RESIDENCES">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta property="og:site_name" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:description" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:image" content="<?php echo base_url();?>assets/images/facebook.jpg"/>
    <meta property="og:url" content="" />
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="<?php echo base_url();?>assets/ico/favicon.png">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
          <div class="panels">
            <div class="logo">
              <img src="<?php echo base_url();?>assets/images/anandamaya-logo.jpg">
            </div>
          </div>
        </div>
      </div>
      <br><br>
      <div class="panels">
        <div class="row">
          <div class="col-lg-12">
            <div class="content">
              <div class="logout text-right">
                <?php if($username !== 'admincs' && $username !== 'adminsafira'){ ?>
                <form action="<?php echo base_url();?>exportallinvoice" method="post">
                  <input type="hidden" name="txtuserid" value="">
                  <input type="submit" value="Export Seluruh Data Rincian Pembayaran" class="btn btn-warning">
                </form><br>
                <a href="<?php echo base_url();?>adduser" class="btn btn-primary">Tambah Data Pembeli</a>
                <?php } ?>
                <a href="<?php echo base_url();?>logout" class="btn btn-danger">Keluar</a>
                <hr>
              </div>
              <div class="title text-center">
                <h2>Halaman Khusus Admin | Rekapitulasi Data Pembeli</h2>
              </div> 

            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
             <div class="content">
               <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tower</th>
                        <th>Nama</th>
                        <th>#</th>
                        <?php if($username !== 'admincs' && $username !== 'adminsafira'){ ?>
                        <th>#</th>
                        <?php }?>
                      </tr>
                    </thead>
                    <tbody>
                    <?php 
                      $counter=1;
                      foreach($qalluser as $qalluser):
                    ?>
                    <tr>
                      <td><?php echo $counter; ?></td>
                      <td><?php echo $qalluser->USERTOWER; ?></td>
                      <td><?php echo $qalluser->USERNAME; ?></td>
                      <td>
                        <form action="<?php echo base_url();?>viewinvoice/" method="post">
                          <input type="hidden" name="txtuserid" value="<?php echo $qalluser->USERID; ?>">
                          <input class="btn btn-default" type="submit" name="viewinvoice" value="Lihat Jadwal Pembayaran">
                        </form>
                      </td>
                      <?php if($username !== 'admincs' && $username !== 'adminsafira'){ ?>
                      <td>
                        <form action="<?php echo base_url();?>deleteuser/" method="post">
                          <input type="hidden" name="txtuserid" value="<?php echo $qalluser->USERID; ?>">
                          <input class="btn btn-default" type="submit" name="delete" value="Hapus" onclick="return confirm('Apa Anda yakin ingin menghapus data pembeli ini?');">
                        </form>
                      </td>
                      <?php } ?>
                    </tr>
                    <?php 
                      $counter++;
                      endforeach;
                    ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="content text-center">
              <div class="footer">
                <br>Copyright &copy; 2015 . Anandamaya Residences . <br><br>
              </div>
             
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/morris/morris.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/js/sb-admin.js"></script>
    <script>
      $(document).ready(function() {
         $('#dataTables-example').dataTable( {
           "paginate": true
         } );
      });
    </script>
  </body>
</html>