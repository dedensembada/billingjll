<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
   
    <title>ANANDAMAYA RESIDENCES</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="ANANDAMAYA RESIDENCES">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta property="og:site_name" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:description" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:image" content="<?php echo base_url();?>assets/images/facebook.jpg"/>
    <meta property="og:url" content="" />
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="<?php echo base_url();?>assets/ico/favicon.png">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div class="row panel">
        <div class="col-md-12">
    <table class="table">
        <tr>
            <th>Tower</td>
            <th>Booking Form</td>
            <th>BF File</td>
            <th>Unit Order Form</td>
            <th>UOF File</td>
        </tr>
        
          <?php foreach($qcekform as $row): ?>
            <tr>
              <td><?php echo $row->USERTOWER;?></td>
              <td><?php echo $row->USERBOOKINGFORM;?></td>
              <td>
                  <?php 
                    $bf = 'assets/images/bookingform/'.$row->USERBOOKINGFORM;
                    if(file_exists($bf)){
                      echo $row->USERBOOKINGFORM;
                    }else{
                        //$userid = $row->USERID;
                        //$updatebf = $this->db->query("UPDATE TUSER SET USERBOOKINGFORM='' WHERE USERID='$userid'");
                    }
                  ?>
              </td>
              <td><?php echo $row->USERUNITORDERFORM;?></td>
              <td>
                <?php 
                    $uof = 'assets/images/unitorderform/'.$row->USERUNITORDERFORM;
                    if(file_exists($uof)){
                      echo $row->USERUNITORDERFORM;
                    }else{
                        //$userid = $row->USERID;
                        //$updateuof = $this->db->query("UPDATE TUSER SET USERUNITORDERFORM='' WHERE USERID='$userid'");
                    }
                  ?>
              </td>
            </tr>
          <?php endforeach; ?>
        
    </table> 
  </div>
  </div>
  </div>
  </body>
</html>