<!DOCTYPE html>
<html lang="en">
  <head>
     <meta charset="utf-8">
   
   <title>ANANDAMAYA RESIDENCES</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="ANANDAMAYA RESIDENCES">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta property="og:site_name" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:description" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:image" content="<?php echo base_url();?>assets/images/facebook.jpg"/>
    <meta property="og:url" content="" />
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="<?php echo base_url();?>assets/ico/favicon.png">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
  </head>
  <body>
    <div class="separator"></div>
    <div class="separator"></div>
	  <div class="container">
      <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
          <div class="panels">
            <div class="logo">
              <img src="<?php echo base_url();?>assets/images/anandamaya-logo.jpg">
            </div>
            <div class="content">
              <form action="<?php echo base_url();?>login" method="post">
                <div class="form-group">
                  <label>Unit No.</label>
                  <input type="text" class="form-control" placeholder="Enter Unit No." name="txtusername">
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" placeholder="Enter Password" name="txtpassword">
                </div>
                <div class="form-group">
                  <label><h6>* Lupa Password :<br>&nbsp;&nbsp;&nbsp;Hub bagian Customer Relation di 021-571 11 11.</h6></label>
                </div>
                <button type="submit" class="btn btn-default">Masuk</button>
                <?php echo (isset($error)?$error:"")?>
              </form>
            </div>
			    </div>
        </div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  </body>
</html>
