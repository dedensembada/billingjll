<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
   
    <title>ANANDAMAYA RESIDENCES</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="ANANDAMAYA RESIDENCES">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta property="og:site_name" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:description" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:image" content="<?php echo base_url();?>assets/images/facebook.jpg"/>
    <meta property="og:url" content="" />
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="<?php echo base_url();?>assets/ico/favicon.png">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div class="row panel">
        <div class="col-md-12">
          <table class="table table-hover">
          <?php 
          
          for($i=1;$i<=36;$i++){
            $like = 'Angsuran '.$i.'/%';
            $query = $this->db->query("SELECT * FROM TINVOICE,TUSER WHERE TINVOICE.USERID = TUSER.USERID AND TINVOICE.INVOICEINFO LIKE '$like' ORDER BY TINVOICE.USERID ASC");
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $qinvoice) {
                    echo "<tr>";
                    echo "<td>".$qinvoice->USERTOWER."</td>";
                    echo "<td>".$qinvoice->INVOICEINFO."</td>";
                    echo "<td>".$qinvoice->INVOICESTATUS."</td>";
                    echo "<td>".$qinvoice->INVOICERECEIPT."</td>";
                    $invoiceid = $qinvoice->INVOICEID;
                    $invoicereceipt = $qinvoice->USERTOWER.'-A'.$i.'.pdf';
                    $qupdate = $this->db->query("UPDATE TINVOICE SET INVOICERECEIPT='$invoicereceipt', INVOICESTATUS='1' WHERE INVOICEID='$invoiceid'");
                    echo "</tr>";
                }
            }
          }
          ?>
          </table>    
        </div>
      </div>
    </div>
  </body>
</html>