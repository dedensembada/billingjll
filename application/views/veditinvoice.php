<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
   
    <title>ANANDAMAYA RESIDENCES</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="ANANDAMAYA RESIDENCES">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta property="og:site_name" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:description" content="ANANDAMAYA RESIDENCES"/>
    <meta property="og:image" content="<?php echo base_url();?>assets/images/facebook.jpg"/>
    <meta property="og:url" content="" />
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="<?php echo base_url();?>assets/ico/favicon.png">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
          <div class="panels">

            <div class="logo">
              <img src="<?php echo base_url();?>assets/images/anandamaya-logo.jpg">
            </div>
          </div>
        </div>
      </div>
      <br><br>
      <div class="panels">
        <div class="row">
          <div class="col-lg-12">
            <div class="content">
              <div class="logout text-right">
                <button onclick="goBack()" class="btn btn-primary">Kembali Ke Rekapitulasi Data Pembeli</button>
                <a href="<?php echo base_url();?>logout" class="btn btn-danger">Keluar</a>
                <hr>
              </div>
              <div class="title text-center">
                <h2>Halaman Khusus Admin | Edit Jadwal Pembayaran</h2>
              </div> 
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-lg-12">
            <div class="content">
              <?php foreach($quser as $quser):?>
              <?php echo form_open_multipart('updateinvoice');?>
                <div class="form-group">
                  <label>Tower</label>
                  <input type="hidden" class="form-control" name="txtuserid" value="<?php echo (isset($quser->USERID)?$quser->USERID:"") ?>">
                  <input type="text" class="form-control" name="txtusertower" value="<?php echo (isset($quser->USERTOWER)?$quser->USERTOWER:"") ?>" readonly>
                </div>
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" name="txtusername" value="<?php echo (isset($quser->USERNAME)?$quser->USERNAME:"") ?>" readonly>
                </div>
                <?php endforeach; ?>
                <?php foreach($qinvoicedetail as $qinvoicedetail): ?>
                <div class="form-group">
                  <label>Keterangan</label>
                  <input type="hidden" class="form-control" name="txtinvoiceid" value="<?php echo (isset($qinvoicedetail->INVOICEID)?$qinvoicedetail->INVOICEID:"") ?>">
                  <input type="text" readonly class="form-control" name="txtinvoiceinfo" value="<?php echo (isset($qinvoicedetail->INVOICEINFO)?$qinvoicedetail->INVOICEINFO:"") ?>">
                </div>
                <div class="form-group">
                  <label>Tgl Jatuh Tempo</label>
                  <input type="text" class="form-control" name="txtinvoiceduedate" value="<?php echo date("d/m/Y",strtotime($qinvoicedetail->INVOICEDUEDATE)); ?>" data-provide="datepicker" class="datepicker" data-date-format="dd/mm/yyyy">
                </div>
                <div class="form-group">
                  <label>Jumlah</label>
                  <input type="text" class="form-control" name="txtinvoicetotal" value="<?php echo (isset($qinvoicedetail->INVOICETOTAL)?$qinvoicedetail->INVOICETOTAL:"") ?>" onkeypress="return isNumber(event)">
                </div>
                <div class="form-group">
                  <label>Denda</label>
                  <!--<input type="text" class="form-control" name="txtinvoicefine" value="<?php echo (isset($qinvoicedetail->INVOICEFINE)?$qinvoicedetail->INVOICEFINE:"") ?>">-->
                  <br>
                  <?php if($qinvoicedetail->INVOICEFINE=='Ada'){ ?>
                  <input type="radio" name="txtinvoicefine" value="Ada" checked> Ada &nbsp;&nbsp;&nbsp;
                  <input type="radio" name="txtinvoicefine" value="Tidak Ada"> Tidak Ada &nbsp;&nbsp;&nbsp;
                  <?php }elseif($qinvoicedetail->INVOICEFINE=="Tidak Ada"){ ?>
                  <input type="radio" name="txtinvoicefine" value="Ada" > Ada &nbsp;&nbsp;&nbsp;
                  <input type="radio" name="txtinvoicefine" value="Tidak Ada" checked> Tidak Ada &nbsp;&nbsp;&nbsp;
                  <?php } ?>
                </div>
                <br>
                <button onclick="goBack()" class="btn btn-primary">Batal</button>
                <button type="submit" class="btn btn-default">Update Jadwal Pembayaran</button>
                <br><br>
                <span style="color:red;"><?php echo (isset($error)?$error:"")?></span>
              </form>
              <?php endforeach; ?> 
            </div>
          </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="content text-center">
              <div class="footer">
                <br>Copyright &copy; 2015 . Anandamaya Residences . <br><br>
              </div>
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
      function goBack() {
          window.history.back()
      }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/morris/morris.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/js/sb-admin.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
    <script>
      $(document).ready(function() {
        $('#dataTables-example').dataTable();
      });
    </script>

    <!-- FUNCTION TEXT FIELD ONLY NUMBER -->
    <script>
      function isNumber(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57)) {
              return false;
          }
          return true;
      }
    </script>

    <script>
      $('.datepicker').datepicker({
          format: 'dd/mm/yyyy',
          startDate: '-3d'
      })
    </script>
  </body>
</html>