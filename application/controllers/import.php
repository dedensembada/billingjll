<?php  
class Import extends CI_Controller {
       function __construct()  {
         parent::__construct();
            $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
    }
 
    function index(){
           if ($this->input->post('save')) {
            $fileName = $_FILES['import']['name'];
 
            $config['upload_path'] = './assets/excel/';
            $config['file_name'] = $fileName;
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size']        = 10000;
 
            $this->load->library('upload');
            $this->upload->initialize($config);
 
            if(! $this->upload->do_upload('import') )
                $this->upload->display_errors();
 
            $media = $this->upload->data('import');
            $inputFileName = './assets/excel/'.$media['file_name'];
 
            //  Read your Excel workbook
            try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
 
            //  Loop through each row of the worksheet in turn
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                //  Insert row data array into your database of choice here
                $data = array(
                            "INVOICETOWER"=> $rowData[0][1],
                            "INVOICEINFO"=> $rowData[0][2],
                            "INVOICEDUEDATE"=> $rowData[0][3],
                            "INVOICETOTAL"=> $rowData[0][4]
                        );
 
                $this->db->insert("TINVOICE",$data);
            }
                        echo "Import Success";
                }
              redirect('dashboard','refresh');
    }
}